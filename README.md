# Copyright and licensing

## RepNumASM

Copyright (c) 2024 Simone De Reggi

RepNumASM is distributed under the terms of the MIT license (see LICENSE.txt).

If you use this software for a scientific publication,
please cite the following paper:

   [1] S. De Reggi, F. Scarabel, R. Vermiglio, ''Approximating reproduction numbers:
         a general numerical method for age-structured models.'' Mathematical Biosciences
         and Engineering, 2024, 21(4): 5360-5393. doi: 10.3934/mbe.2024236
         
   [2] S. De Reggi, F. Scarabel, R. Vermiglio, ''On the convergence of the pseudospectral
         approximation of reproduction numbers for age-structured models.'' (submitted).

# RepNumASM

RepNumASM is a package for the numerical approximation of the reproduction numbers of a wide class of linear age-structured population models (both from ecology and epidemiology). 

The repository contains the MATLAB codes for the numerical approximation of the reproduction numbers of linear age-structured population models formulated as integro-partial differential equations with non-local boundary conditions according to the method of [1,2]. 
This package allows complete flexibility in the choice of the ''birth'' and ''transition'' processes. RepNumASM computes the pseudospectral discretization of the reformulation via integration of the age-state of the birth and transition operators B and M, namely BN and MN. Then, the approximations of the relevant reproduction number is given by the spectral radius of BN*MN^(-1).

The package is provided for giving the possibility to reproduce the experiments contained in [1,2] by using the following codes.

# AS_Ex1.m

Computes the reproduction number relevant to Example 1 in [2].

# TSI.m

Computes the reproduction number relevant to Example 2 in [2].

# TSI_Isol.m

Computes the reproduction number relevant to model (4.1) in [1].

# Symp_Asymp.m

Computes the reproduction number relevant to model (4.3) in [1]

# Rubella.m

Computes the reproduction number relevant to model (4.4) in [1]

# HBV.m

Computes the reproduction number relevant to Example 3 in  [2]

# HBV_bifdiag.m

Produces the bifurcation diagram for Example 3 in [2]

## Description of the code

## RepNumASM.m

RepNumASM computes the pseudospectral discretization of the reformulation via integration of the age-state of the birth and transition operators of a class of linear age-structured population models formulated as integro-partial differential equations with nonlocal boundary conditions.

-----------

## difmatZeros.m
Computes the differentiation matrix relevant to the Chebysbhev zeros extended by the left endpoint in [-1, 1]

-----------

## difmatExt.m
Computes the differentiation matrix relevant to the Chebysbhev extremal nodes in [-1, 1]

-----------

## fclencurt.m
Computes the Chebyshev extremal nodes and the Clenshaw-Curtis quadrature weights on a given interval.

-----------

## difmatZeros.m

Copyright (c) 2000 Lloyd N. Trefethen

The code in difmatZeros.m is taken from L. N. Trefethen, Spectral Methods in MATLAB, SIAM, 2000, DOI: 10.1137/1.9780898719598.

Even though the codes therein are not explicitly licensed, considering that they are made available for download on the author's web page and that 
variations of some of them are included in Chebfun, which is distributed under the 3-clause BSD license, I believe that the author's intention was 
for his codes to be freely used and that distributing this file together with RepNumASM does not violate his rights.

-----------

## difmatExt.m

Copyright (c) 2000 Lloyd N. Trefethen

The code in difmatExt.m is taken from L. N. Trefethen, Spectral Methods in MATLAB, SIAM, 2000, DOI: 10.1137/1.9780898719598.

Even though the codes therein are not explicitly licensed, considering that they are made available for download on the author's web page and that 
variations of some of them are included in Chebfun, which is distributed under the 3-clause BSD license, I believe that the author's intention was 
for his codes to be freely used and that distributing this file together with RepNumASM does not violate his rights.

-----------

## fclencurt.m

fclencurt, version 1.0.0.0, 02/12/2005, by Greg von Winckel

Copyright (c) 2009, Greg von Winckel

Retrieved on November 17, 2021 from https://www.mathworks.com/matlabcentral/fileexchange/6911-fast-clenshaw-curtis-quadrature

fclencurt is distributed under the terms of the 2-clause BSD license 
