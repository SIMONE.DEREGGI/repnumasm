function [D, w, W] = difmatZeros(xpoint)
  
% Computes the differentiation matrices relevant to the Chebyshev zeros 
% extened by the left endpoint in xpoint according to [1, 2].
%
% INPUT:
%     xpoint = vector containing the Chebyshev zeros extended by the left
%                   endpoint in the interval [-1, 1]
%                                
% OUTPUT:
%         DL = differentiation matrix relevant to xpoint 
%     
% CALL:
%       >>DL = difmat(xpoint)
%
%
% REFERENCES: 
%            [1] J.P. Berrut,  L.N. Trefethen, "Barycentric lagrange interpolation,"
%                SIAM review 46.3 (2004): 501-517, DOI: 10.1137/S0036144502417715.
%            [2] L.N. Trefethen, "Spectral methods in Matlab", SIAM, 2000, 
%                DOI: 10.1137/1.9780898719598.

N = length(xpoint) - 1; 
J = 1:N;
JJ = 0:N-1;

w = ((-1).^JJ).*sin( ((2*J) - 1)*pi/(2*N) );
w = flip(w);

ell = prod( -1*ones(1, N) - xpoint(2:end) );

W = w./(xpoint(2:end)+ones(1, N));
W = [ ell^(-1) ,  2^(N-1)*N^(-1)*W];

X = repmat(xpoint', 1, N+1);
dX = X - X';
D =((1./W)'*W)./( dX + eye(N+1) );
D = D - diag( sum(D') );    

end