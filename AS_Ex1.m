function R = AS_Ex1(R0, index, N, S)

%   Copyright (c) 2023-2024 Simone De Reggi.
%
%   If you use this software for a scientific publication,
%   please cite the following papers:
%
%   [1] S. De Reggi, F. Scarabel, R. Vermiglio, ''Approximating reproduction numbers:
%         a general numerical method for age-structured models.'' Mathematical Biosciences
%         and Engineering, 2024, 21(4): 5360-5393. doi: 10.3934/mbe.2024236
%   [2] S. De Reggi, F. Scarabel, R. Vermiglio, ''On the convergence of the pseudospectral
%         approximation of reproduction numbers for age-structured models.'' (submitted).
%
%   Refers to Example 1 in [2]
%
%   R0 is the reference value of the basic reproduction number
%
%   index == 1 for analytic case
%   index == 2 for C^\infty case
%   index == 3 for W^{3, \infty} case
%
%   if index ~= 1, it sets amax = gamma = 1
%   otherwise, it requires to set either amax == 1, amax == 10 or amax == 30
%   and gamma == 1, gamma == 10 or gamma == 100.
%
%   N+1 is the number of nodes to be used in the pseudospectral discretization
%
%   Put S==0 for the discretization at the Chebyshev zeros
%   or S==1 for for the discretization at Chebyshev extrema

if index ~= 1
    amax = 1;
    gamma = 1;
else
    prompt = " What is the value of amax? ";
    amax = input(prompt);
    if amax ~= 1 && amax ~= 10 && amax ~= 30
        fprintf('Wrong value of amax. Automatically setted to amax = 1\n');
        amax = 1;
    end
    prompt = " What is the value of gamma? ";
    gamma = input(prompt);
    if gamma ~= 1 && gamma ~= 10 && gamma ~= 100
        fprintf('Wrong value of gamma. Automatically setted to amax = 1\n');
        gamma = 1;
    end
end

if S ~= 0 && S  ~= 1
    S = 0;
    fprintf('Wrong value of S. Automatically setted to S = 0\n');
end

switch index
    case 1
        q = @(x) exp(-2*x);
        switch gamma
            case 1
                switch amax
                    case 1
                        c = - (1 + (-4 + exp(1))*exp(1))/(2*exp(2));
                    case 10
                        c = (17 - exp(-20) + 4/exp(10))/20;
                    case 30
                        c = (57 - exp(-60) + 4/exp(30))/60;
                end
            case 10
                switch amax
                    case 1
                        c = (16 - exp(-10) + 25/exp(2))/400;
                    case 10
                        c = (376 - exp(-100) + 25/exp(20))/4000;
                    case 30
                        c = (1176 - exp(-300) + 25/exp(60))/12000;
                end
            case 100
                switch amax
                    case 1
                        c = (2401 - exp(-100) + 2500/exp(2))/490000;
                    case 10
                        c = (46501 - exp(-1000) + 2500/exp(20))/4900000;
                    case 30
                        c = (144501 - exp(-3000) + 2500/exp(60))/14700000;
                end
        end
    case 2
        q = @(x) (0.5-x).^2*abs(0.5-x);
        c = 2*0.0049334349836081574897971;
    case 3
        q =@(x) NotAnalytic(x);
        c = 0.027731423149657;
end

c = c/amax;

a=[0, amax]; 
z=[]; 
d=1;

delta={@(x) - gamma};
b1 = {@(x) 0};
b2 = {@(x) 0};
b = {b1, b2};
beta1 = {@(x, y) c^(-1)*R0*q(x)*(amax-y)*2/(amax)^2};
beta2 = {@(x, y) 0};
beta = {beta1, beta2};

if S == 1
    [BN, MN] = RepNumASM(a, z, d, b, delta, beta, N, 1);
else
    [BN, MN] = RepNumASM(a, z, d, b, delta, beta, N, 0);
end

e = sort(eig(BN/MN), 'ComparisonMethod','real');
R=e(end);

end

%% C^\infty example

function [q] = NotAnalytic(s)

if s <=0.2
    q = 0;
else
    q = exp(-1/(s-0.2))*1/(s-0.2).^2;
end
    
end