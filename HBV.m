function R = HBV(nu, theta, N, S, varargin)

%   Copyright (c) 2023-2024 Simone De Reggi.
%
%   If you use this software for a scientific publication,
%   please cite the following papers:
%
%   [1] S. De Reggi, F. Scarabel, R. Vermiglio, ''Approximating reproduction numbers:
%         a general numerical method for age-structured models.'' Mathematical Biosciences
%         and Engineering, 2024, 21(4): 5360-5393. doi: 10.3934/mbe.2024236
%   [2] S. De Reggi, F. Scarabel, R. Vermiglio, ''On the convergence of the pseudospectral
%         approximation of reproduction numbers for age-structured models.'' (submitted).
%
%   Refers to Example 3 in [2]
%
%   nu in [0, infty) per capita vaccination rate at age a>0
%   omega in [0, 1] fraction failed vaccination at birth
%
%   N+1 is the number of nodes to be used in the pseudospectral discretization
%
%   Put S==0 for the discretization at the Chebyshev zeros
%   or S==1 for for the discretization at Chebyshev extrema
%
%   HBV(q, varargin) default computes R_0
%   if varargin == 2, it computes T_H
%   if varargin == 3, it computes T_V

if S ~= 0 && S  ~= 1
    S = 0;
    fprintf('Wrong value of S. Automatically setted to S = 0\n');
end

if isempty(varargin) || (varargin{1} ~= 2 && varargin{1} ~= 3)
    index = 1;
elseif varargin{1}==2
    index = 2;
elseif varargin{1}==3
    index = 3;
end

a = [0, 75];   
z = [3, 6, 10, 15, 18, 30, 50]; 
d=3;

% specific parameter definition

p = @(x) 0.176501*exp(-0.787711*x)+0.02116;
epsilon = 0.16;
sigma = 6;
gamma1 = 4;
gamma2 = 0.025;
omega = 0.1;
q1 = 0.711;
q2 = 0.109;

if nu == 0
    theta = 1;
end

s = @(x) Sus(x, omega, nu, theta);

[Kij]=gamma1*HBV_WAIFW_Matrix;

delta={@(x) [-sigma, 0, 0; sigma, -gamma1, 0; 0, p(x)*gamma1, -gamma2],...
    @(x) [-sigma, 0, 0; sigma, -gamma1, 0; 0, p(x)*gamma1, -gamma2],...
    @(x) [-sigma, 0, 0; sigma, -gamma1, 0; 0, p(x)*gamma1, -gamma2],...
    @(x) [-sigma, 0, 0; sigma, -gamma1, 0; 0, p(x)*gamma1, -gamma2],...
    @(x) [-sigma, 0, 0; sigma, -gamma1, 0; 0, p(x)*gamma1, -gamma2],...
    @(x) [-sigma, 0, 0; sigma, -gamma1, 0; 0, p(x)*gamma1, -gamma2],...
    @(x) [-sigma, 0, 0; sigma, -gamma1, 0; 0, p(x)*gamma1, -gamma2],...
    @(x) [-sigma, 0, 0; sigma, -gamma1, 0; 0, p(x)*gamma1, -gamma2]};

switch index
    case 1
        b1={@(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, theta*q1*0.018, theta*q2*0.018; 0, 0, 0; 0, 0, 0],...
            @(x) [0, theta*q1*0.018, theta*q2*0.018; 0, 0, 0; 0, 0, 0],...
            @(x) [0, theta*q1*0.018, theta*q2*0.018; 0, 0, 0; 0, 0, 0]};
        b2={@(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, 0, 0; 0, 0, 0; 0, 0, 0]};
        beta1={@(x, y) s(x)*[0, Kij(1, 1), epsilon*Kij(1, 1); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(1, 2), epsilon*Kij(1, 2); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(1, 3), epsilon*Kij(1, 3); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(1, 4), epsilon*Kij(1, 4); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(1, 5), epsilon*Kij(1, 5); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(1, 5), epsilon*Kij(1, 5); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(1, 6), epsilon*Kij(1, 6); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(1, 7), epsilon*Kij(1, 7); 0, 0, 0; 0, 0, 0];...
            @(x, y) s(x)*[0, Kij(2, 1), epsilon*Kij(2, 1); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(2, 2), epsilon*Kij(2, 2); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(2, 3), epsilon*Kij(2, 3); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(2, 4), epsilon*Kij(2, 4); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(2, 5), epsilon*Kij(2, 5); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(2, 5), epsilon*Kij(2, 5); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(2, 6), epsilon*Kij(2, 6); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(2, 7), epsilon*Kij(2, 7); 0, 0, 0; 0, 0, 0];...
            @(x, y) s(x)*[0, Kij(3, 1), epsilon*Kij(3, 1); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(3, 2), epsilon*Kij(3, 2); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(3, 3), epsilon*Kij(3, 3); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(3, 4), epsilon*Kij(3, 4); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(3, 5), epsilon*Kij(3, 5); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(3, 5), epsilon*Kij(3, 5); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(3, 6), epsilon*Kij(3, 6); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(3, 7), epsilon*Kij(3, 7); 0, 0, 0; 0, 0, 0];...
            @(x, y) s(x)*[0, Kij(4, 1), epsilon*Kij(4, 1); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(4, 2), epsilon*Kij(4, 2); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(4, 3), epsilon*Kij(4, 3); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(4, 4), epsilon*Kij(4, 4); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(4, 5), epsilon*Kij(4, 5); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(4, 5), epsilon*Kij(4, 5); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(4, 6), epsilon*Kij(4, 6); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(4, 7), epsilon*Kij(4, 7); 0, 0, 0; 0, 0, 0];...
            @(x, y) s(x)*[0, Kij(5, 1), epsilon*Kij(5, 1); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(5, 2), epsilon*Kij(5, 2); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(5, 3), epsilon*Kij(5, 3); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(5, 4), epsilon*Kij(5, 4); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(5, 5), epsilon*Kij(5, 5); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(5, 5), epsilon*Kij(5, 5); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(5, 6), epsilon*Kij(5, 6); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(5, 7), epsilon*Kij(5, 7); 0, 0, 0; 0, 0, 0];...
            @(x, y) s(x)*[0, Kij(5, 1), epsilon*Kij(5, 1); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(5, 2), epsilon*Kij(5, 2); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(5, 3), epsilon*Kij(5, 3); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(5, 4), epsilon*Kij(5, 4); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(5, 5), epsilon*Kij(5, 5); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(5, 5), epsilon*Kij(5, 5); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(5, 6), epsilon*Kij(5, 6); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(5, 7), epsilon*Kij(5, 7); 0, 0, 0; 0, 0, 0];...
            @(x, y) s(x)*[0, Kij(6, 1), epsilon*Kij(6, 1); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(6, 2), epsilon*Kij(6, 2); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(6, 3), epsilon*Kij(6, 3); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(6, 4), epsilon*Kij(6, 4); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(6, 5), epsilon*Kij(6, 5); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(6, 5), epsilon*Kij(6, 5); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(6, 6), epsilon*Kij(6, 6); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(6, 7), epsilon*Kij(6, 7); 0, 0, 0; 0, 0, 0];...
            @(x, y) s(x)*[0, Kij(7, 1), epsilon*Kij(7, 1); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(7, 2), epsilon*Kij(7, 2); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(7, 3), epsilon*Kij(7, 3); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(7, 4), epsilon*Kij(7, 4); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(7, 5), epsilon*Kij(7, 5); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(7, 5), epsilon*Kij(7, 5); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(7, 6), epsilon*Kij(7, 6); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(7, 7), epsilon*Kij(7, 7); 0, 0, 0; 0, 0, 0]};        
        beta2={@(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0];...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0];...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0];...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0];...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0];...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0];...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0];...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0]};
    case 2        
        b1={@(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, 0, 0; 0, 0, 0; 0, 0, 0]};
        b2={@(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, theta*q1*0.018, theta*q2*0.018; 0, 0, 0; 0, 0, 0],...
            @(x) [0, theta*q1*0.018, theta*q2*0.018; 0, 0, 0; 0, 0, 0],...
            @(x) [0, theta*q1*0.018, theta*q2*0.018; 0, 0, 0; 0, 0, 0]};
        beta1={@(x, y) s(x)*[0, Kij(1, 1), epsilon*Kij(1, 1); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(1, 2), epsilon*Kij(1, 2); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(1, 3), epsilon*Kij(1, 3); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(1, 4), epsilon*Kij(1, 4); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(1, 5), epsilon*Kij(1, 5); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(1, 5), epsilon*Kij(1, 5); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(1, 6), epsilon*Kij(1, 6); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(1, 7), epsilon*Kij(1, 7); 0, 0, 0; 0, 0, 0];...
            @(x, y) s(x)*[0, Kij(2, 1), epsilon*Kij(2, 1); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(2, 2), epsilon*Kij(2, 2); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(2, 3), epsilon*Kij(2, 3); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(2, 4), epsilon*Kij(2, 4); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(2, 5), epsilon*Kij(2, 5); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(2, 5), epsilon*Kij(2, 5); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(2, 6), epsilon*Kij(2, 6); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(2, 7), epsilon*Kij(2, 7); 0, 0, 0; 0, 0, 0];...
            @(x, y) s(x)*[0, Kij(3, 1), epsilon*Kij(3, 1); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(3, 2), epsilon*Kij(3, 2); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(3, 3), epsilon*Kij(3, 3); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(3, 4), epsilon*Kij(3, 4); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(3, 5), epsilon*Kij(3, 5); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(3, 5), epsilon*Kij(3, 5); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(3, 6), epsilon*Kij(3, 6); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(3, 7), epsilon*Kij(3, 7); 0, 0, 0; 0, 0, 0];...
            @(x, y) s(x)*[0, Kij(4, 1), epsilon*Kij(4, 1); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(4, 2), epsilon*Kij(4, 2); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(4, 3), epsilon*Kij(4, 3); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(4, 4), epsilon*Kij(4, 4); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(4, 5), epsilon*Kij(4, 5); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(4, 5), epsilon*Kij(4, 5); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(4, 6), epsilon*Kij(4, 6); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(4, 7), epsilon*Kij(4, 7); 0, 0, 0; 0, 0, 0];...
            @(x, y) s(x)*[0, Kij(5, 1), epsilon*Kij(5, 1); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(5, 2), epsilon*Kij(5, 2); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(5, 3), epsilon*Kij(5, 3); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(5, 4), epsilon*Kij(5, 4); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(5, 5), epsilon*Kij(5, 5); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(5, 5), epsilon*Kij(5, 5); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(5, 6), epsilon*Kij(5, 6); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(5, 7), epsilon*Kij(5, 7); 0, 0, 0; 0, 0, 0];...
            @(x, y) s(x)*[0, Kij(5, 1), epsilon*Kij(5, 1); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(5, 2), epsilon*Kij(5, 2); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(5, 3), epsilon*Kij(5, 3); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(5, 4), epsilon*Kij(5, 4); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(5, 5), epsilon*Kij(5, 5); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(5, 5), epsilon*Kij(5, 5); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(5, 6), epsilon*Kij(5, 6); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(5, 7), epsilon*Kij(5, 7); 0, 0, 0; 0, 0, 0];...
            @(x, y) s(x)*[0, Kij(6, 1), epsilon*Kij(6, 1); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(6, 2), epsilon*Kij(6, 2); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(6, 3), epsilon*Kij(6, 3); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(6, 4), epsilon*Kij(6, 4); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(6, 5), epsilon*Kij(6, 5); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(6, 5), epsilon*Kij(6, 5); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(6, 6), epsilon*Kij(6, 6); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(6, 7), epsilon*Kij(6, 7); 0, 0, 0; 0, 0, 0];...
            @(x, y) s(x)*[0, Kij(7, 1), epsilon*Kij(7, 1); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(7, 2), epsilon*Kij(7, 2); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(7, 3), epsilon*Kij(7, 3); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(7, 4), epsilon*Kij(7, 4); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(7, 5), epsilon*Kij(7, 5); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(7, 5), epsilon*Kij(7, 5); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(7, 6), epsilon*Kij(7, 6); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(7, 7), epsilon*Kij(7, 7); 0, 0, 0; 0, 0, 0]};        
        beta2={@(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0];...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0];...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0];...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0];...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0];...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0];...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0];...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0]};
    case 3
        b1={@(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, theta*q1*0.018, theta*q2*0.018; 0, 0, 0; 0, 0, 0],...
            @(x) [0, theta*q1*0.018, theta*q2*0.018; 0, 0, 0; 0, 0, 0],...
            @(x) [0, theta*q1*0.018, theta*q2*0.018; 0, 0, 0; 0, 0, 0]};
        b2={@(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x) [0, 0, 0; 0, 0, 0; 0, 0, 0]};
        beta1={@(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0];...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0];...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0];...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0];...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0];...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0];...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0];...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0],...
            @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0], @(x, y) [0, 0, 0; 0, 0, 0; 0, 0, 0]};        
        beta2={@(x, y) s(x)*[0, Kij(1, 1), epsilon*Kij(1, 1); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(1, 2), epsilon*Kij(1, 2); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(1, 3), epsilon*Kij(1, 3); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(1, 4), epsilon*Kij(1, 4); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(1, 5), epsilon*Kij(1, 5); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(1, 5), epsilon*Kij(1, 5); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(1, 6), epsilon*Kij(1, 6); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(1, 7), epsilon*Kij(1, 7); 0, 0, 0; 0, 0, 0];...
            @(x, y) s(x)*[0, Kij(2, 1), epsilon*Kij(2, 1); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(2, 2), epsilon*Kij(2, 2); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(2, 3), epsilon*Kij(2, 3); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(2, 4), epsilon*Kij(2, 4); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(2, 5), epsilon*Kij(2, 5); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(2, 5), epsilon*Kij(2, 5); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(2, 6), epsilon*Kij(2, 6); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(2, 7), epsilon*Kij(2, 7); 0, 0, 0; 0, 0, 0];...
            @(x, y) s(x)*[0, Kij(3, 1), epsilon*Kij(3, 1); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(3, 2), epsilon*Kij(3, 2); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(3, 3), epsilon*Kij(3, 3); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(3, 4), epsilon*Kij(3, 4); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(3, 5), epsilon*Kij(3, 5); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(3, 5), epsilon*Kij(3, 5); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(3, 6), epsilon*Kij(3, 6); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(3, 7), epsilon*Kij(3, 7); 0, 0, 0; 0, 0, 0];...
            @(x, y) s(x)*[0, Kij(4, 1), epsilon*Kij(4, 1); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(4, 2), epsilon*Kij(4, 2); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(4, 3), epsilon*Kij(4, 3); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(4, 4), epsilon*Kij(4, 4); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(4, 5), epsilon*Kij(4, 5); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(4, 5), epsilon*Kij(4, 5); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(4, 6), epsilon*Kij(4, 6); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(4, 7), epsilon*Kij(4, 7); 0, 0, 0; 0, 0, 0];...
            @(x, y) s(x)*[0, Kij(5, 1), epsilon*Kij(5, 1); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(5, 2), epsilon*Kij(5, 2); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(5, 3), epsilon*Kij(5, 3); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(5, 4), epsilon*Kij(5, 4); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(5, 5), epsilon*Kij(5, 5); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(5, 5), epsilon*Kij(5, 5); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(5, 6), epsilon*Kij(5, 6); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(5, 7), epsilon*Kij(5, 7); 0, 0, 0; 0, 0, 0];...
            @(x, y) s(x)*[0, Kij(5, 1), epsilon*Kij(5, 1); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(5, 2), epsilon*Kij(5, 2); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(5, 3), epsilon*Kij(5, 3); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(5, 4), epsilon*Kij(5, 4); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(5, 5), epsilon*Kij(5, 5); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(5, 5), epsilon*Kij(5, 5); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(5, 6), epsilon*Kij(5, 6); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(5, 7), epsilon*Kij(5, 7); 0, 0, 0; 0, 0, 0];...
            @(x, y) s(x)*[0, Kij(6, 1), epsilon*Kij(6, 1); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(6, 2), epsilon*Kij(6, 2); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(6, 3), epsilon*Kij(6, 3); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(6, 4), epsilon*Kij(6, 4); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(6, 5), epsilon*Kij(6, 5); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(6, 5), epsilon*Kij(6, 5); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(6, 6), epsilon*Kij(6, 6); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(6, 7), epsilon*Kij(6, 7); 0, 0, 0; 0, 0, 0];...
            @(x, y) s(x)*[0, Kij(7, 1), epsilon*Kij(7, 1); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(7, 2), epsilon*Kij(7, 2); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(7, 3), epsilon*Kij(7, 3); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(7, 4), epsilon*Kij(7, 4); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(7, 5), epsilon*Kij(7, 5); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(7, 5), epsilon*Kij(7, 5); 0, 0, 0; 0, 0, 0],...
            @(x, y) s(x)*[0, Kij(7, 6), epsilon*Kij(7, 6); 0, 0, 0; 0, 0, 0], @(x, y) s(x)*[0, Kij(7, 7), epsilon*Kij(7, 7); 0, 0, 0; 0, 0, 0]};        
end

b={b1, b2};
beta={beta1, beta2};

if S == 1
    [BN, MN] = RepNumASM(a, z, d, b, delta, beta, N, 1);
else
    [BN, MN] = RepNumASM(a, z, d, b, delta, beta, N, 0);
end

e = sort(eig(BN/MN), 'ComparisonMethod','real');
R=e(end);

end

%% Density of susceptibles at disease free equilibrium

function [s] = Sus(a, omega, nu, theta)

%   Computes the density of susceptibles individuals at the
%   disease--free equilibrium for Example 3 in [1].
%
%   [1] S. De Reggi, F. Scarabel, R. Vermiglio, ''On the convergence of the pseudospectral
%         approximation of reproduction numbers for age-structured models.'' (submitted).

if nu==0
    s=1;
else
    s = theta*exp(-(omega+nu)*a)+omega/(omega+nu)*(1-exp(-(omega+nu)*a));
end
end

%% WAIFW matrix

function [K]=HBV_WAIFW_Matrix

%   Computes the WAIFW matrix relevant to Example 3
%   in [1] using the method of [2, Appendix A]
%
%   [1] S. De Reggi, F. Scarabel, R. Vermiglio, ''On the convergence of the pseudospectral
%         approximation of reproduction numbers for age-structured models.'' (submitted).
%   [2] R. Anderson and R.May, ''Age-related changes  in the rate of disease
%        transmission: implications for the design of vaccination programmes'',
%        Epidemiol. Infect., 94 (1985), 365-436


a=[0, 3, 6, 10, 15, 30, 50, 75];
lambda=[(Intfoi(a(2))-Intfoi(a(1)))/(a(2)-a(1)), (Intfoi(a(3))-Intfoi(a(2)))/(a(3)-a(2)),...
    (Intfoi(a(4))-Intfoi(a(3)))/(a(4)-a(3)), (Intfoi(a(5))-Intfoi(a(4)))/(a(5)-a(4)),...
    (Intfoi(a(6))-Intfoi(a(5)))/(a(6)-a(5)), (Intfoi(a(7))-Intfoi(a(6)))/(a(7)-a(6)),...
    (Intfoi(a(8))-Intfoi(a(7)))/(a(8)-a(7))];
n=size(a, 2); 
n=n-1;
psi=ones(1, n+1);
psi(1)=0;
for i=2:n+1
    psi(i)=0;
    j=2;
    while j<=i
        psi(i)=psi(i)+lambda(j-1)*(a(j)-a(j-1));
        j=j+1;
    end
end
PSI=ones(1, n);
for i=1:n
   PSI(i)=exp(-psi(i))-exp(-psi(i+1)); 
end
S=sum(PSI);
A=[PSI(1), PSI(2), PSI(3), PSI(4), PSI(5), PSI(6), PSI(7);...
   0, PSI(1)+PSI(2), PSI(3), PSI(4), PSI(5), PSI(6), PSI(7);...
    0, 0, PSI(1)+PSI(2)+PSI(3), PSI(4), PSI(5), PSI(6), PSI(7);...
    0, 0, 0, S-PSI(5)-PSI(6)-PSI(7), PSI(5), PSI(6), PSI(7);...
    0, 0, 0, 0, S-PSI(6)-PSI(7), PSI(6), PSI(7);...
    0, 0, 0, 0, 0, S-PSI(7), PSI(7);...
    0, 0, 0, 0, 0, 0, S];
k=A^(-1)*(lambda');
K=[k(1), k(2), k(3), k(4), k(5), k(6), k(7);...
     k(2), k(2), k(3), k(4), k(5), k(6), k(7);...
     k(3), k(3), k(3), k(4), k(5), k(6), k(7);...
     k(4), k(4), k(4), k(4), k(5), k(6), k(7);...
     k(5), k(5), k(5), k(5), k(5), k(6), k(7);...
     k(6), k(6), k(6), k(6), k(6), k(6), k(7);...
     k(7), k(7), k(7), k(7), k(7), k(7), k(7)];

end

%% Catalytic force of infection

function [s] = foi(a)

% Catalytic force of infection in [2, Formula 2]
%
% [2] S. Zhao, Z. Xu and Y. Lu, ''A mathematical model of hepatitis B virus transmission and
%      its application for vaccination strategy in China'', Int. J. Epidemiol., 29 (2000), 744-752

if a<=47.5
    s=0.13074116-0.01362531*a+0.00046463*a^2-0.00000489*a^3;
else
    s=0.13074116-0.01362531*(47.5)+0.00046463*(47.5)^2-0.00000489*(47.5)^3;
end

end

%% Integral of the catalytic force of infection

function [s] = Intfoi(a)

% Computes the integral of the catalytic force of infection in [2, Formula 2]
%
% [2] S. Zhao, Z. Xu and Y. Lu, ''A mathematical model of hepatitis B virus transmission and
%      its application for vaccination strategy in China'', Int. J. Epidemiol., 29 (2000), 744-752

if a<=47.5
    s=0.13074116*a-0.006812655000000*a.^2+1.548766666666667e-04...
      *a^3-1.222500000000000e-06*a^4;
else
    s=(0.13074116*47.5-0.006812655000000*(47.5).^2+1.548766666666667e-04...
      *(47.5)^3-1.222500000000000e-06*(47.5)^4)+foi(47.5)*(a-47.5);
end

end