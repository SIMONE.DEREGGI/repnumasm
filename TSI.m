function R = TSI(R0, N, S, varargin)

%   Copyright (c) 2023-2024 Simone De Reggi.
%
%   If you use this software for a scientific publication,
%   please cite the following papers:
%
%   [1] S. De Reggi, F. Scarabel, R. Vermiglio, ''Approximating reproduction numbers:
%         a general numerical method for age-structured models.'' Mathematical Biosciences
%         and Engineering, 2024, 21(4): 5360-5393. doi: 10.3934/mbe.2024236
%   [2] S. De Reggi, F. Scarabel, R. Vermiglio, ''On the convergence of the pseudospectral
%         approximation of reproduction numbers for age-structured models.'' (submitted).
%
%   Refers to Example 2 in [2]
%
%   R0 is the reference value of the basic reproduction number
%
%   N+1 is the number of nodes to be used in the pseudospectral discretization
%
%   Put S==0 for the discretization at the Chebyshev zeros
%   or S==1 for for the discretization at Chebyshev extrema
%
%   TSI(R0, N, S, varargin) default uses shape paramater for the Gamma
%   density equal to  2. 
%
%   if varargin = pi, uses shape paramater equal to  pi. 

if S ~= 0 && S  ~= 1
    S = 0;
    fprintf('Wrong value of S. Automatically setted to S = 0\n');
end

if isempty(varargin) || (varargin{1} ~= pi)
    shape_par = 2; 
else
    shape_par = pi; 
end

a=[0, 14];
z=7;
d=1;

% Gamma density
scale_par = 0.25;
exponent =  shape_par-1; 
gamma0 = 4;

% Transmission rate
b_function = @(x) R0/(gamma(shape_par)*...
    scale_par^(shape_par)).*(x.^(exponent))*(1/gamcdf(14, shape_par, scale_par));

mu0 = gamma0;
delta={@(x) -mu0, @(x) -mu0 };
b1={@(x) b_function(x), @(x) b_function(x)};
b2={@(x) 0, @(x) 0};
b={b1, b2};
beta1={@(x, y) 0, @(x, y) 0;...
    @(x, y) 0, @(x, y) 0};
beta2={@(x, y) 0, @(x, y) 0;...
    @(x, y) 0, @(x, y) 0};
beta={beta1, beta2};

if S == 1
    [BN, MN] = RepNumASM(a, z, d, b, delta, beta, N, 1);
else
    [BN, MN] = RepNumASM(a, z, d, b, delta, beta, N, 0);
end

e = sort(eig(BN/MN), 'ComparisonMethod','real');
R=e(end);

end