function HBV_bifdiag

%   If you use this software for a scientific publication,
%   please cite the following papers:
%
%   [1] S. De Reggi, F. Scarabel, R. Vermiglio, ''Approximating reproduction numbers:
%         a general numerical method for age-structured models.'' Mathematical Biosciences
%         and Engineering, 2024, 21(4): 5360-5393. doi: 10.3934/mbe.2024236
%   [2] S. De Reggi, F. Scarabel, R. Vermiglio, ''On the convergence of the pseudospectral
%         approximation of reproduction numbers for age-structured models.'' (submitted).
%
%   Produces the bifurcation diagram for Example 3 in [2]

N = 50;
nu=linspace(0, 0.2, 40);
omega=linspace(0, 1, 40);

[V, O]=meshgrid(nu, omega);

R= zeros(40);

for i=1:size(O, 1)
    i
    for j=1:size(O, 2)
        j
        R(i,j) = HBV(V(i,j), O(i,j), N, 0);
    end
end

subplot(1,2,1)
surf(V, O, R)
hold on
contour3(V, O, R, [1,1], 'ShowText','on','edgecolor','k', 'LineWidth',2)
axis square
xlim([0.01, 0.2])
ax = gca;
ax.FontSize = 17;
xlabel('$\nu$', 'interpreter', 'latex', 'fontsize', 30)
ylabel('$\theta$', 'interpreter', 'latex', 'fontsize', 30)

subplot(1,2,2)
[C, h] = contour(V, O, R, 'LineWidth',3);
clabel(C,h,'FontSize', 15)
hold on
[C,h] = contour(V, O, R, [1, 1], 'k',  'LineWidth',3);
clabel(C,h,'FontSize',15,'Color','k')
axis square
xlim([0.01, 0.2])
ax = gca;
ax.FontSize = 17;
xlabel('$\nu$', 'interpreter', 'latex', 'fontsize', 30)
ylabel('$\theta$', 'interpreter', 'latex', 'fontsize', 30)
title('$R_0$', 'interpreter', 'latex', 'fontsize', 30)
colorbar 

end