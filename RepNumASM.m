function [BN, MN] = RepNumASM(a, z, d, b, delta, beta,  N, varargin)

%   Copyright (c) 2023-2024 Simone De Reggi.
%   RepNumASM.m is distributed under the terms of the MIT license.
%
%   If you use this software for a scientific publication,
%   please cite the following papers:
%
%   [1] S. De Reggi, F. Scarabel, R. Vermiglio, ''Approximating reproduction numbers:
%         a general numerical method for age-structured models.'' Mathematical Biosciences
%         and Engineering, 2024, 21(4): 5360-5393. doi: 10.3934/mbe.2024236
%   [2] S. De Reggi, F. Scarabel, R. Vermiglio, ''On the convergence of the pseudospectral
%         approximation of reproduction numbers for age-structured models.'' (submitted).
%
%   [BN, MN] = RepNumASM(a, z, d, beta, u, k, alpha, N)
%   computes the pseudospectral discretization of the reformulation via
%   integration of the age-state of the birth and transition operators according
%   to the the method described in [1] using a piecewise approach with N
%   Chebyshev zeros extended by the left endpoint in each interval [s(i), s(i+1)],
%   where s=[a(1), z, a(end)].
%
%  INPUT:
%              a = age interval
%              z = row vector containing the ordered breaking points of the
%                    coefficients of the model. Put z=[] if no breaking
%                    points occur.
%              d = dimension of the system
%              b = {b1, b2}, where b1 and b2 are cell arrays of dimension
%                     1 x (p+1), where p = length(z),  containing functions of
%                     one variable with values in \R^{d\times d}
%
%                     b1 correspond to b_+  and b2 correspond to b_- in [1, 2]
%                     b1 interpreted as birth part, b2 interpreted as transition
%
%               Example: for d=2 and p=1
%                                beta1 = {[@f11(x), @f12(x); f21(x), @f22(x) ]},
%
%           delta = is a cell array of dimension 1 x (p+1), where p = length(z),
%                     containing functions of one variable with values in \R^{d\times d}
%
%               Example: for d=2 and p=1
%                                delta={[@g11(x), @g12(x); g21(x), @g22(x) ]},
%
%            beta = {beta1, beta2} where beta1 and beta2 are cell arrays of dimension
%                     (p+1) x (p+1), where p = length(z), containing functions of
%                     two variables with values in \R^{d\times d}
%
%                     beta1 correspond to \beta_+  and beta2 correspond to \beta_- in [1]
%                     beta1 interpreted as birth part, beta2 interpreted as transition
%
%               Example: for d=1 and p=1
%                                beta1={ [@f11(x), @f12(x); f21(x), @f22(x) ],  [@g11(x), @g12(x); g21(x), @g22(x) ] ;
%                                              [@ff11(x), @ff12(x); ff21(x), @ff22(x) ], [@gg11(x), @gg12(x); gg21(x), @gg22(x) ]}
%
%         N+1 = number of nodes to be used in the pseudospectral discretization
%
%   Default discretization uses N Chebyshev zeros extended by the left endpoint
%
%   Put varargin == 1 to obtain the discretization at the N+1 Chebyshev Extrema
%
%   In the case of Chebyshev extrema, we use the code fclencurt.m by [3] to
%   compute the nodes and the Clenshaw--Curtis quadrature weights.
%
%    [3] Greg von Winckel (2021). Fast Clenshaw-Curtis Quadrature
%         (https://www.mathworks.com/matlabcentral/fileexchange/
%         6911-fast-clenshaw-curtis-quadrature), MATLAB Central 
%         File Exchange. Retrieved November 16, 2021.
%
%
%  OUTPUT:
%          BN = discretization of the integrated version of the birth
%                    operator
%          MN = discretization of the integrated version of the transition
%                    operator
%
%  CALL:
%        >>[BN, MN] =  RepNumASM(a, z, d, b, delta, beta,  N)
%

%% setting the use of either Chebyshev zeros or extrema

if isempty(varargin) || varargin{1}==1
    index = 1;
else
    index  = 0;
end

if index == 0
%% Discretization at Chebyshev zeros extended by the left endpoint
    
    z=[a(1), z, a(end)];
    l=size(z, 2);
    
    % This will contain the Chebyshev zeros extended by the left endpoint
    X=ones(l-1, N+1);
    % This will contain the Fejer's first quadrature rule weights
    QW=ones(l-1, N+1);
    % This will contain the differentiation matrices
    Diff = cell(1, l-1);
    
    % Chebyshev zeros extended by left endpoint and Fejer's first rule quadrature weights in [-1, 1]
    x = - sin( ( N+1 - 2*(1:N) )*pi/(2*N) );
    x = [ -1 , x ];
    xi = ( 2*(1:N) - 1 )*pi/(2*N);
    J = 1 : floor(N/2);
    qw = ones(1, N);
    for k = 1 : N
        qw(k) = 2/N*( 1 - 2*sum( cos( 2*J*xi(k) )./(4*J.^2 - 1) ) );
    end
    qw = [0, qw];
    
    % Differentiation matrix
    [D, ~, W] = difmatZeros(x);
    
    y=(x+ones(1, N+1))/2;
    
    % Nodes, weights and differentiation matrices in each interval [z(i), z(i+1)]
    for i=1:l-1
        X(i,:) = y*(z(i+1)-z(i))+z(i)*ones(1, N+1);
        QW(i,:) = qw*(z(i+1)-z(i))/2;
        Diff{1, i} = (2/(z(i+1)-z(i)))*D;
    end
    
    % Derivative operator. Needs to evaluate the polynomial at the
    % right-endpoint in each interval [z(i), z(i+1)] for continuity in the
    % piecewise approach.
    
    if l-2>0
        L = zeros(1, N+1);
        for j = 1:N+1
            L(j) =  W(j)/( X(2, 1) - X(1, j) );
        end
        L = L /sum( L ) ;
        L = repmat(L, l-2, 1);
    end
    
    D = cell(1, l-1);
    
    for i = 1:l-1
        D{1, i} = [];
        j = i;
        k = 2;
        while l-1 > l-j
            D{1, i} = vertcat( D{1, i} , zeros(N) );
            j = j - 1;
            k = k + 1;
        end
        D{1, i} = vertcat( D{1, i} , Diff{1, i}( 2:end , 2:end ) );
        if i < l-1
            while k <= l-1
                D{1, i} = vertcat( D{1, i} , L( i, 2:end ).*Diff{1, k}( 2:end, 1) );
                k = k+1;
            end
        end
    end
    
    LL = ones(l-1);
    
    if l - 3 > 0
        for j = 1 : l-3
            for i = (2 + j) : l-1
                LL( i, j ) = L( i - 2, 1)*LL( i - 1, j);
            end
        end
    end
    
    DD = horzcat(D{1, :});
    
    LL = kron(LL, ones(N));
    
    DD = LL.*DD;
    
    % Integration matrix (inverse of derivative operator)
    
    II = DD^(-1);
    
    % Differentiation and integration matrices of suitable dimension
    
    DDDD = kron(DD, eye(d));
    
    IIII= kron(II, eye(d));
    
    % Updating X and QW
    
    X = X( : , 2:end);
    QW = QW( : , 2:end);
    
    XX=reshape(X', [] ,1);
    
%%%%%  Birth Operator %%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % B1 matrix
    
    b1=b{1,1};
    B1=[];
    
    for i=1:l-1
        bb1=b1{1, i};
        for j=1:N
            B1=horzcat( B1, bb1( X(i, j) )*QW(i, j) );
        end
    end
    
    B1=repmat(B1, N*(l-1), 1)*DDDD;
    
    % BETA1 matrix
    
    beta1=beta{1,1};
    BETA1=cell(N*(l-1), 1);
    
    for i=1:N*(l-1)
        BETA1{i, 1}=[];
    end
    
    h=1;
    p=1;
    s=1;
    
    while h<=N*(l-1)
        if mod(h, N)==1 && h~=1
            s=s+1;
        end
        for i = 1:l-1
            betabeta1=beta1{s, i};
            for j=1:N
                BETA1{p, 1}= horzcat( BETA1{p, 1}, betabeta1(XX(h), X(i, j))*QW(i,j) );
            end
        end
        h=h+1;
        p=p+1;
    end
    
    BETABETA1=[BETA1{1,1}];
    
    for i=2:N*(l-1)
        BETABETA1=vertcat( BETABETA1, BETA1{i,1} );
    end
    
    BETABETA1=BETABETA1*DDDD;
    
    BETA1=IIII*BETABETA1;
    
    % Builds the birth operator
    
    BN=BETA1+B1;
    
    
%%%%%  Transition Operator %%%%%%%%%%%%%%%%%%%%%%
    
    % DELTA matrix
    
    DELTA=[];
    
    for i=1:l-1
        deltadelta=delta{1, i};
        for j = 1:N
            vec = deltadelta(X(i, j));                    % N blocks d\times d
            DELTA = horzcat(DELTA, vec);
        end
    end
    
    cc = kron(eye( N*(l-1) ), ones(d));
    DELTA = repmat(DELTA, N*(l-1), 1 );
    
    DELTA = DELTA.*cc;
    
    DELTA=IIII*DELTA*DDDD;
    
    % B2 matrix
    
    b2=b{1,2};
    B2=[];                               
    
    for i=1:l-1
        bb2=b2{1, i};
        for j=1:N
            B2=horzcat( B2, bb2(X(i, j))*QW(i,j) );
        end
    end
    
    B2=repmat(B2, N*(l-1), 1)*DDDD;
    
    % BETA2 matrix
    
    beta2=beta{1,2};
    BETA2=cell(N*(l-1), 1);
    
    for i=1:N*(l-1)
        BETA2{i, 1}=[];                                    
    end
    
    h=1;
    p=1;
    s=1;
    
    while h<=N*(l-1)
        if mod(h, N)==1 && h~=1
            s=s+1;
        end
        for i=1:l-1
            betabeta2=beta2{s, i};
            for j=1:N
                BETA2{p, 1}=horzcat( BETA2{p, 1}, betabeta2(XX(h), X(i, j))*QW(i,j) );
            end
        end
        h=h+1;
        p=p+1;
    end
    
    BETABETA2=[BETA2{1,1}];
    
    for i=2:N*(l-1)
        BETABETA2=vertcat( BETABETA2, BETA2{i,1} );
    end
    
    BETABETA2=BETABETA2*DDDD;
    
    BETA2=IIII*BETABETA2;
    
    % Builds the transition operator
    
    MN = DDDD - DELTA - BETA2 - B2;
    
else   
%% Discretization at Chebyshev extrema %%%%%%%%
    
    N = N+1;
    z=[a(1), z, a(end)];
    l=size(z, 2);
    
    % This will contain the Chebyshev extrema
    X=ones(l-1, N);
    % This will contain the Clenshaw--Curtis weights
    W=ones(l-1, N);
    % This will contain the differentiation matrices
    Diff = cell(1, l-1);

    
    % Chebyshev extrema and Clenshaw--Curtis weights
    [x,w] = fclencurt(-1, 1, N);
    % Differentiation matrix
    D = difmatExt(x);
    
    y=(x+ones(1, N))/2;
    
    % Nodes, weights and differentiation matrices in each interval [z(i), z(i+1)]
    for i=1:l-1
        X(i,:)=y*(z(i+1)-z(i))+z(i)*ones(1, N);
        W(i,:)=w*(z(i+1)-z(i))/2;
        Diff{1, i} = (2/(z(i+1)-z(i)))*D;
    end
    
    XX=reshape(X',[],1);
    
    % Derivative operator
    
    D(1:N, 1:N) = Diff{1, 1};
    
    for i = 2:(l-1)
        D = vertcat( horzcat( D, zeros(N+(N-1)*(i-2), N-1) ), horzcat( zeros(N-1, (N-1)*(i-1) ), Diff{1, i}(2:end, :) ) );
    end
    
    % Derivative operator imposing functions zero at a=0
    
    DD = D(2:end, 2:end);
    
    % Integration matrix/inverse of derivative operator
    
    II = DD^(-1);
    
    % Diff and Int matrix of correct dimension
    
    DDD = kron(D, eye(d));
    DDDD = kron(DD, eye(d));
    IIII= kron(II, eye(d));
    
%%%%%  Birth Operator %%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % B1 matrix
    
    b1=b{1,1};
    B1=[];                                                 
    BB1=zeros(d, d*N*(l-1)-d*(l-2));     
    
    
    for i=1:l-1
        bb1=b1{1, i};
        for j=1:N
            vec=bb1(X(i, j))*W(i,j);      
            B1 = horzcat(B1, vec);
        end
    end
    
    i=1;
    j=1;
    
    while i <= d*N*(l-1)
        BB1(:, j)=B1(:, i);
        t=mod(i, N*d);
        if (t>=(N-1)*d+1 || t==0) && i<=d*N*(l-2)
            BB1(:, j)=BB1(:, j)+B1(:, i+d);
        end
        if t==0
            i=i+d;
        end
        i=i+1;
        j=j+1;
    end
    
    BB1=repmat(BB1, (N*(l-1)-(l-2)), 1)*DDD;
    BB1=BB1(d+1:end, d+1:end);
    
    
    % BETA1 matrix
    
    beta1=beta{1,1};
    BETA1=cell(N*(l-1)-(l-2), 1);
    BETABETA1=BETA1;
    
    for i=1:N*(l-1)-(l-2)
        BETA1{i, 1}=[];                                             % N blocks d\times d
        BETABETA1{i, 1}=zeros(d, d*N*(l-1)-d*(l-2));
    end
    
    h=1;
    p=1;
    s=1;
    
    while h<=N*(l-1)
        if mod(h, N)==1 && h~=1
            h=h+1;
            s=s+1;
        end
        for i=1:l-1
            betabeta1=beta1{s, i};
            for j=1:N
                c1=betabeta1(XX(h), X(i, j))*W(i,j);     % N blocks d\times d
                BETA1{p, 1} = horzcat( BETA1{p, 1}, c1 );
            end
        end
        h=h+1;
        p=p+1;
    end
    
    h=1;
    p=1;
    
    while h<=N*(l-1)
        if mod(h, N)==1 && h~=1
            h=h+1;
        end
        i=1;
        j=1;
        while i <= d*N*(l-1)
            BETABETA1{p, 1}(:, j)=BETA1{p, 1}(:, i);
            t=mod(i, N*d);
            if (t>=(N-1)*d+1 || t==0) && i<=d*N*(l-2)
                BETABETA1{p, 1}(:, j)=BETABETA1{p, 1}(:, j)+BETA1{p, 1}(:, i+d);
            end
            if t==0
                i=i+d;
            end
            i=i+1;
            j=j+1;
        end
        h=h+1;
        p=p+1;
    end
    
    BETA1=[BETABETA1{1,1}];
    
    for i=2:N*(l-1)-(l-2)
        BETA1=vertcat(BETA1, BETABETA1{i,1});
    end
    
    BETA1=BETA1*DDD;
    BETABETA1=BETA1(d+1:end, d+1:end);
    BETABETA1=IIII*BETABETA1;
    
    % Builds the birth operator
    
    BN=BETABETA1+BB1;
    
    
%%%%%  Transition Operator %%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % DELTA matrix
    
    DELTA=[];                                             
    DELTADELTA=zeros(d, d*( N*(l-1) - (l-2) ));
    
    for i=1:l-1
        deltadelta=delta{1, i};
        for j=1:N
            vec=deltadelta(X(i, j));           
            DELTA=horzcat(DELTA, vec);
        end
    end
    
    i=1;
    j=1;
    
    while i <= d*N*(l-1)
        DELTADELTA(:, j)=DELTA(:, i);
        t=mod(i, N*d);
        if t==0
            i=i+d;
        end
        i=i+1;
        j=j+1;
    end
    
    cc=kron(eye(N*(l-1)-(l-2)), ones(d));
    
    DELTA=zeros((N*(l-1)-(l-2))*d);
    
    for i=1:N*(l-1)-(l-2)
        for j=1:d
            DELTA((i-1)*d+j, :)=cc((i-1)*d+j, :).*DELTADELTA(j, :);
        end
    end
    
    DELTADELTA=DELTA(d+1:end, d+1:end);
    DELTADELTA=IIII*DELTADELTA*DDDD;
    
    % B2 matrix
    
    b2=b{1,2};
    
    B2=[];                                                
    BB2=zeros(d, d*N*(l-1)-d*(l-2));
    
    for i=1:l-1
        bb2=b2{1, i};
        for j=1:N
            vec=bb2(X(i, j))*W(i,j);       
            B2=horzcat(B2, vec);
        end
    end
    
    i=1;
    j=1;
    
    while i <= d*N*(l-1)
        BB2(:, j)=B2(:, i);
        t=mod(i, N*d);
        if (t>=(N-1)*d+1 || t==0) && i<=d*N*(l-2)
            BB2(:, j)=BB2(:, j)+B2(:, i+d);
        end
        if t==0
            i=i+d;
        end
        i=i+1;
        j=j+1;
    end
    
    BB2=repmat(BB2, (N*(l-1)-(l-2)), 1)*DDD;
    BB2=BB2(d+1:end, d+1:end);
    
    
    % BETA2 matrix
    
    beta2=beta{1,2};
    
    BETA2=cell(N*(l-1)-(l-2), 1);
    BETABETA2=BETA2;
    
    for i=1:N*(l-1)-(l-2)
        BETA2{i, 1}=[];                                          
        BETABETA2{i, 1}=zeros(d, d*N*(l-1)-d*(l-2));
    end
    
    h=1;
    p=1;
    s=1;
    
    while h<=N*(l-1)
        if mod(h, N)==1 && h~=1
            h=h+1;
            s=s+1;
        end
        for i=1:l-1
            betabeta2=beta2{s, i};
            c2=cell(1, N);
            for j=1:N
                c2{1, j}=betabeta2(XX(h), X(i, j))*W(i,j);
                BETA2{p, 1}=horzcat(BETA2{p, 1}, c2{1,j} );
            end
        end
        h=h+1;
        p=p+1;
    end
    
    h=1;
    p=1;
    
    while h<=N*(l-1)
        if mod(h, N)==1 && h~=1
            h=h+1;
        end
        i=1;
        j=1;
        while i <= d*N*(l-1)
            BETABETA2{p, 1}(:, j)=BETA2{p, 1}(:, i);
            t=mod(i, N*d);
            if (t>=(N-1)*d+1 || t==0) && i<=d*N*(l-2)
                BETABETA2{p, 1}(:, j)=BETABETA2{p, 1}(:, j)+BETA2{p, 1}(:, i+d);
            end
            if t==0
                i=i+d;
            end
            i=i+1;
            j=j+1;
        end
        h=h+1;
        p=p+1;
    end
    
    BETA2=[BETABETA2{1,1}];
    
    for i=2:N*(l-1)-(l-2)
        BETA2=vertcat( BETA2, BETABETA2{i,1} );
    end
    
    BETA2=BETA2*DDD;
    BETABETA2=BETA2(d+1:end, d+1:end);
    BETABETA2=IIII*BETABETA2;
    
    
% Builds the transition operator
    
    MN = DDDD-DELTADELTA-BETABETA2-BB2;
end

end