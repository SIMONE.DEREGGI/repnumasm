function R = Symp_Asymp(r, N, S, varargin)

%   Copyright (c) 2023-2024 Simone De Reggi.
%
%   If you use this software for a scientific publication,
%   please cite the following papers:
%
%   [1] S. De Reggi, F. Scarabel, R. Vermiglio, ''Approximating reproduction numbers:
%         a general numerical method for age-structured models.'' Mathematical Biosciences
%         and Engineering, 2024, 21(4): 5360-5393. doi: 10.3934/mbe.2024236
%   [2] S. De Reggi, F. Scarabel, R. Vermiglio, ''On the convergence of the pseudospectral
%         approximation of reproduction numbers for age-structured models.'' (submitted).
%
%   Refers to Example 4.3 in [1]
%
%   r is a varying parameter representing the spectral radius of the NGO
%   relevant to asymptomatic transmission
%
%   N+1 is the number of nodes to be used in the pseudospectral discretization
%
%   Put S==0 for the discretization at the Chebyshev zeros
%   or S==1 for for the discretization at Chebyshev extrema
%
%   Symp_Asymp(r, N, varargin) default computes R_0
%   if varargin == 2, it computes T_S
%   if varargin == 3, it computes the spectral radius of
%   the NGO relevant to asympomatic individuals

if S ~= 0 && S  ~= 1
    S = 0;
    fprintf('Wrong value of S. Automatically setted to S = 0\n');
end

if isempty(varargin) || (varargin{1} ~= 2 && varargin{1} ~= 3)
    index = 1;
elseif varargin{1}==2
    index = 2;
elseif varargin{1}==3
    index = 3;
end

a=[0,14];
z=[]; 
d=2;

% specific parameter definition
gamma1=0;
gamma2=0.45;
b12=0.0695;
b21=0.676;
b11=r*b21;

delta={@(x) [-b21-gamma1, 0 ; 0, -gamma2]};
beta1={@(x, y) [0, 0; 0, 0]};
beta2={@(x, y) [0, 0; 0, 0]};
beta={beta1, beta2};

switch index
    case 1
        b1={@(x) [b11, b12; 0, 0]};
        b2={@(x) [0, 0; b21, 0]};
        b={b1, b2};
    case 2
        b1={@(x) [0, 0; b21, 0]};
        b2={@(x) [b11, b12; 0, 0]};
        b={b1, b2};
    case 3
        b1={@(x) [b11, 0; 0, 0]};
        b2={@(x) [0, 0; 0, 0]};
        b={b1, b2};        
end

if S == 1
    [BN, MN] = RepNumASM(a, z, d, b, delta, beta, N, 1);
else
    [BN, MN] = RepNumASM(a, z, d, b, delta, beta, N, 0);
end

e = sort(eig(BN, MN), 'ComparisonMethod','real');
R=e(end);

end