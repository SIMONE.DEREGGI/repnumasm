function R = TSI_Isol(R0, D, N, S, varargin)

%   Copyright (c) 2023-2024 Simone De Reggi.
%
%   If you use this software for a scientific publication,
%   please cite the following papers:
%
%   [1] S. De Reggi, F. Scarabel, R. Vermiglio, ''Approximating reproduction numbers:
%         a general numerical method for age-structured models.'' Mathematical Biosciences
%         and Engineering, 2024, 21(4): 5360-5393. doi: 10.3934/mbe.2024236
%   [2] S. De Reggi, F. Scarabel, R. Vermiglio, ''On the convergence of the pseudospectral
%         approximation of reproduction numbers for age-structured models.'' (submitted).
%
%   Refers to model 4.1 in [1]
%
%   D in [0, 14] delay between symptoms onset and isolation 
%
%   N+1 is the number of nodes to be used in the pseudospectral discretization
%
%   Put S==0 for the discretization at the Chebyshev zeros
%   or S==1 for for the discretization at Chebyshev extrema
%
%   R = TSI(R0, D, varargin), default computes R_0, 
%          i.e.,  considers epsilon = 0
%   
%   if 0<varargin <=1, it computes R_c (control reproduction number)
%
%   D is greater than 0, uses a piecewise approach breaking the interval [0, 14] into
%   [0, D] and [D, 14]

if S ~= 0 && S  ~= 1
    S = 0;
    fprintf('Wrong value of S. Automatically setted to S = 0\n');
end

if isempty(varargin) 
    epsilon = 0;
elseif 0<=varargin{1} && varargin{1}<=1    
    epsilon = varargin{1};
else
    disp('Error! $\epsilon$ must be in the interval [0, 1]')
    epsilon = NaN;
end

a=[0, 14];
d=1;

%specific parameters definition

% Gamma distribution with mean 4.84 and standard  deviation 2.79
mean_inc = 4.84;
sigma_inc = 2.79;
scale_inc = sigma_inc^2/mean_inc;
shape_inc = mean_inc/scale_inc;

% Gamma distribution with mean 5 and standard  deviation 1.9
mean_omega=5;
sigma_omega=1.9;
scale_omega = sigma_omega^2/mean_omega;
shape_omega = mean_omega/scale_omega;
exponent=(mean_omega^2/sigma_omega^2)-1;
gamma0 = mean_omega/(sigma_omega^2);

% Transmission rate
b_function = @(x) R0/(gamma(shape_omega)*scale_omega^(shape_omega)).*...
    (x.^(exponent))*(1/gamcdf(14, shape_omega,scale_omega));

% Rate at which infected are getting diagnosed  
rate_diag = @(x) epsilon*gampdf(x-D,shape_inc,scale_inc)./(1-epsilon*gamcdf(x-D,shape_inc,scale_inc));

if D>0 && D <14
    z=D;
    mu0 = gamma0;
    delta={@(x) -mu0, @(x) -(mu0 + rate_diag(x))};
    b1={@(x) b_function(x), @(x) b_function(x)};
    b2={@(x) 0, @(x) 0};
    b={b1, b2};
    beta1={@(x, y) 0, @(x, y) 0;...
                 @(x, y) 0, @(x, y) 0};
    beta2={@(x, y) 0, @(x, y) 0;...
                 @(x, y) 0, @(x, y) 0};
    beta={beta1, beta2};
elseif D==0    
    z=[];
    mu0 = gamma0;
    delta={@(x) -(mu0 + rate_diag(x))};
    b1={@(x) b_function(x)};
    b2={@(x) 0};
    b={b1, b2};
    beta1={@(x, y) 0};
    beta2={@(x, y) 0};
    beta={beta1, beta2};
elseif D == 14
    z=[];
    mu0 = gamma0;
    delta={@(x) -mu0};
    b1={@(x) b_function(x)};
    b2={@(x) 0};
    b={b1, b2};
    beta1={@(x, y) 0};
    beta2={@(x, y) 0};
    beta={beta1, beta2};
end

if S == 1
    [BN, MN] = RepNumASM(a, z, d, b, delta, beta, N, 1);
else
    [BN, MN] = RepNumASM(a, z, d, b, delta, beta, N, 0);
end

e = sort(eig(BN/MN), 'ComparisonMethod','real');
R=e(end);

end