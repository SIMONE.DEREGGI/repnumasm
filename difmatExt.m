function DL = difmatExt(xpoint)
  
% Computes the differentiation matrices relevant to the Chebyshev extremal 
% points contained in xpoint according to [1, 2].
%
% INPUT:
%     xpoint = vector containing the Chebyshev extremal nodes in
%                   in the interval [-1, 1]
%                                
% OUTPUT:
%         DL = differentiation matrix relevant to xpoint 
%     
% CALL:
%       >>DL = difmat(xpoint)
%
%
% REFERENCES: 
%            [1] L.N. Trefethen, "Spectral methods in Matlab", SIAM, 2000, 
%                DOI: 10.1137/1.9780898719598.
%            [2] J.P. Berrut,  L.N. Trefethen, "Barycentric lagrange interpolation,"
%                SIAM review 46.3 (2004): 501-517, DOI: 10.1137/S0036144502417715.

N = length(xpoint)-1;
x=xpoint';
c=[2;ones(N-1,1);2].*(-1).^(0:N)';
X=repmat(x,1,N+1);
dX=X-X';
DL=(c*(1./c)')./(dX+(eye(N+1)));
DL=DL-diag(sum(DL'));    

end